import sqlalchemy
from sqlalchemy import MetaData, Table, String, Integer, Column, Text, Boolean, create_engine
from datetime import datetime
from sqlalchemy import insert, select

engine = create_engine('sqlite://sqlite3.db')

metadata = MetaData()

blog = Table('blog', metadata,
             Column('id', Integer()),
             Column('name', String(50))
             )

ins = blog.insert().values(
    id="10",
    name="Damir"
)

print(ins)

conn = engine.connect()
r = conn.execute(ins)
