from aiogram.dispatcher.filters.state import StatesGroup, State


class Shop(StatesGroup):
    start = State()
    catalog = State()
    cart = State()
    address = State()