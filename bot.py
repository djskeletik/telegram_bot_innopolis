# from config import TOKEN
# from aiogram import Bot, Dispatcher, types, executor
# from aiogram.types import ParseMode
# from utils import Form
# from aiogram.dispatcher import FSMContext
# import logging
# import aiogram.utils.markdown as md
#
# from aiogram.contrib.fsm_storage.memory import MemoryStorage
# from aiogram.contrib.middlewares.logging import LoggingMiddleware
#
#
# bot = Bot(token=TOKEN)
# dp = Dispatcher(bot, storage=MemoryStorage())
# dp.middleware.setup(LoggingMiddleware())
#
# logging.basicConfig(level=logging.INFO)
#
# @dp.message_handler(commands='start')
# async def cmd_start(message: types.Message):
#     await Form.name.set()
#     await message.reply("Hi! What's your name?")
#
# @dp.message_handler(state='*', commands='cancel')
# async def cancel_handler(message: types.Message, state: FSMContext):
#
#     current_state = await state.get_state()
#     if current_state is None:
#         return
#
#     logging.info('Cancelling state %r', current_state)
#     await state.finish()
#     await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())
#
# @dp.message_handler(state=Form.name)
# async def process_name(message: types.Message, state: FSMContext):
#     async with state.proxy() as data:
#         data['name'] = message.text
#
#     await Form.age.set()
#     await message.reply("How old are you?")
#
# @dp.message_handler(lambda message: not message.text.isdigit(), state=Form.age)
# async def process_age_invalid(message: types.Message):
#     await message.reply("Age gotta be a number. \nHow old, are you? (digits only)")
#
# @dp.message_handler(lambda message: message.text.isdigit(), state=Form.age)
# async def process_age(message: types.Message, state:FSMContext):
#     await Form.gender.set()
#     await state.u


# @dp.message_handler(commands=['start'])
# async def send_welcome(message: types.Message):
#     await bot.send_message(message.from_user.id, "Hey, how are you?", reply_markup=mk.Hay_manu)
#
# @dp.message_handler()
# async def otvetochka(message: types.Message):
#     button_text = message.text
#
#     if button_text == "amazing":
#         reply_text = "that's great"
#     elif button_text == "good_from aiogram import Bot, Dispatcher, executor, types
# from aiogram.contrib.middlewares.logging import LoggingMiddlewared":
#         reply_text = "great"
#     elif button_text == "Ok":
#         reply_text = "KAIF"
#     elif button_text == "bad":
#         reply_text = "That's bad"
#     else:
#         reply_text = 'uhmmmm....'
#     await message.answer(reply_text, reply_markup=ReplyKeyboardRemove())
# executor.start_polling(dp, skip_updates=True)
#
#
#
# from aiogram import Bot, Dispatcher, executor, types
#
# from config import TOKEN
# from aiogram.types import ParseMode
# from utils import Form
# from aiogram.dispatcher import FSMContext
# import logging
# import aiogram.utils.markdown as md
#
# from aiogram.contrib.fsm_storage.memory import MemoryStorage
# from aiogram.contrib.middlewares.logging import LoggingMiddleware
#
#
# bot = Bot(token=TOKEN)
# dp = Dispatcher(bot, storage=MemoryStorage())
# dp.middleware.setup(LoggingMiddleware())
#
# logging.basicConfig(level=logging.INFO)
#
#
# @dp.message_handler(commands='start')
# async def cmd_start(message: types.Message):
#     # Set state
#     await Form.name.set()
#
#     await message.reply("Hi there! What's your name?")
#
#
# # You can use state '*' if you need to handle all states
# @dp.message_handler(state='*', commands='cancel')
# async def cancel_handler(message: types.Message, state: FSMContext):
#
#     current_state = await state.get_state()
#     if current_state is None:
#         return
#
#     logging.info('Cancelling state %r', current_state)
#     # Cancel state and inform user about it
#
#     await state.finish()
#     # And remove keyboard (just in case)
#     await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())
#
#
# @dp.message_handler(state=Form.name)
# async def process_name(message: types.Message, state: FSMContext):
#     async with state.proxy() as data:
#         data['name'] = message.text
#
#     await Form.age.set()
#     await message.reply("How old are you?")
#
#
# # Check age. Age gotta be digit
# @dp.message_handler(lambda message: not message.text.isdigit(), state=Form.age)
# async def process_age_invalid(message: types.Message):
#     await message.reply("Age gotta be a number.\nHow old are you? (digits only)")
#
#
# @dp.message_handler(lambda message: message.text.isdigit(), state=Form.age)
# async def process_age(message: types.Message, state: FSMContext):
#     # Update state and data
#     await Form.gender.set()
#     await state.update_data(age=int(message.text))
#
#     # Configure ReplyKeyboardMarkup
#     markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
#     markup.add("Male", "Female")
#     markup.add("Other")
#
#     await message.reply("What is your gender?", reply_markup=markup)
#
#
# @dp.message_handler(lambda message: message.text not in ["Male", "Female", "Other"], state=Form.gender)
# async def process_gender_invalid(message: types.Message):
#     return await message.reply("Bad gender name. Choose your gender from the keyboard.")
#
#
# @dp.message_handler(state=Form.gender)
# async def process_gender(message: types.Message, state: FSMContext):
#     async with state.proxy() as data:
#         data['gender'] = message.text
#
#         markup = types.ReplyKeyboardRemove()
#
#         await bot.send_message(
#             message.chat.id,
#             md.text(
#                 md.text('Hi! Nice to meet you,', md.bold(data['name'])),
#                 md.text('Age:', md.code(data['age'])),
#                 md.text('Gender:', data['gender']),
#                 sep='\n',
#             ),
#             reply_markup=markup,
#             parse_mode=ParseMode.MARKDOWN,
#         )
#
#     # Finish conversation
#     await state.finish()
#
#
#
# if __name__ == '__main__':
#     executor.start_polling(dp, skip_updates=True)

from aiogram import Bot, Dispatcher, executor, types

from config import TOKEN
from aiogram.types import ParseMode
from utils import Shop
from aiogram.dispatcher import FSMContext
import logging
import aiogram.utils.markdown as md

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands='start')
async def cmd_start(message: types.Message):
    await Shop.catalog.set()

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Kartoshka", "Markovka", "My cart")

    await message.reply("Chto vy hotite kupit'?", reply_markup=markup)


@dp.message_handler(state=Shop.cart)
async def cart(message: types.Message, state: FSMContext):
    aswer_data = message.text
    if aswer_data == "Buy":
        await Shop.address.set()
        await message.reply(f"Adress pozaza", reply_markup=types.ReplyKeyboardRemove())
    else:
        print("YES")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Kartoshka", "Markovka", "My cart")
        await message.reply("Chto vy hotite kupit'?", reply_markup=markup)
        await Shop.catalog.set()


@dp.message_handler(state=Shop.address)
async def adress(message: types.Message, state: FSMContext):
    aswer_data = message.text
    await Shop.address.set()
    await message.reply(f"Ваш адрес - {aswer_data}")
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Монстр", "РедБулл", "My cart")
    await message.reply("", reply_markup=markup)
    async with state.proxy() as data:
        data['products'] = None
    await Shop.catalog.set()


@dp.message_handler(state=Shop.catalog)
async def catalog(message: types.Message, state: FSMContext):
    aswer_data = message.text
    if aswer_data != "My cart":
        await message.reply(f"В корзину добавлен {aswer_data!r}")
        async with state.proxy() as data:
            l = []
            if data.get('products'):
                for i in data.get('products'):
                    l.append(i)
            l.append(aswer_data)
            data['products'] = l
        print(data.get('products'))
    else:
        await Shop.cart.set()
        keyboard_markup = types.ReplyKeyboardMarkup(row_width=3)
        button_text = ("Вернуться в магазин", "Оформить покупку")
        keyboard_markup.row(*(types.KeyboardButton(text) for text in button_text))
        reply_text = "Your cart contains: \n"
        async with state.proxy() as data:
            if data.get('products'):
                for i in data.get('products'):
                    reply_text += i + "\n"
                    print(i)
            else:
                reply_text = "Вы ничего не купили"
        await message.reply(reply_text, reply_markup=keyboard_markup)


executor.start_polling(dp, skip_updates=True)
