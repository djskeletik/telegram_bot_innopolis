from aiogram import Bot, Dispatcher, executor, types
from config import TOKEN, PROXY_URL
from utils import Shop
from aiogram.dispatcher import FSMContext
import logging
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=TOKEN, proxy=PROXY_URL)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands='start')
async def start(message: types.Message):
    await Shop.catalog.set()

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Monster", "Redbull", "Coffe", "My cart")

    await message.reply("Какие продукты вы предпочитаете?", reply_markup=markup)


@dp.message_handler(state=Shop.cart)
async def cart(message: types.Message, state: FSMContext):
    answer_data = message.text
    if answer_data == "Оформить":
        await Shop.address.set()
        await message.reply(f"Куда за вами выезжать?", reply_markup=types.ReplyKeyboardRemove())
    else:
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
        markup.add("Monster", "Redbull", "Coffe", "My cart")
        await message.reply("Какие продукты вы предпочитаете?", reply_markup=markup)
        await Shop.catalog.set()


@dp.message_handler(state=Shop.address)
async def address(message: types.Message, state: FSMContext):
    answer_text = message.text
    await Shop.address.set()
    await message.reply(f"Мы выехали за вами на адрес - {answer_text}")
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
    markup.add("Monster", "Redbull", "Coffe", "My cart")
    await message.reply("Какие продукты вы предпочитаете?", reply_markup=markup)
    async with state.proxy() as data:
        data['products'] = None
    await Shop.catalog.set()


@dp.message_handler(state=Shop.catalog)
async def catalog(message: types.Message, state: FSMContext):
    aswer_data = message.text
    if aswer_data != "My cart":
        await message.reply(f"В корзину добавлено: {aswer_data!r}")
        async with state.proxy() as data:
            l = []
            if data.get('products'):
                for i in data.get('products'):
                    l.append(i)
            l.append(aswer_data)
            data['products'] = l
        print(data.get('products'))
    else:
        await Shop.cart.set()
        keyboard_markup = types.ReplyKeyboardMarkup(row_width=3)
        button_text = ("Вернуться ", "Оформить")
        keyboard_markup.row(*(types.KeyboardButton(text) for text in button_text))
        reply_text = "В корзине: \n"
        async with state.proxy() as data:
            if data.get('products'):
                for i in data.get('products'):
                    reply_text += i + "\n"
                    print(i)
            else:
                reply_text = "Вы ничего не купили"
        await message.reply(reply_text, reply_markup=keyboard_markup)
executor.start_polling(dp, skip_updates=True)