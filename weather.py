import json
import aiohttp
import asyncio
import config
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware
import logging

bot = Bot(token=config.TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands="money")
async def get_coord(message: types.Message):
    async with aiohttp.ClientSession() as session:
        print(message.text)
        async with session.get(
                f"https://currate.ru/api/?get=rates&pairs={message.text.split()[1]}&key={config.MONEY_TOKEN}") as resp:
            print(resp)
            json_obj = json.loads(str(await resp.text()))
            print(json_obj)
            if json_obj["data"][message.text.split()[1]] is None:
                await message.reply("Ошибка запроса")
            await message.reply(json_obj["data"][message.text.split()[1]])


executor.start_polling(dp, skip_updates=True)
