from aiogram import Bot, Dispatcher, executor, types

from config import TOKEN
from aiogram.types import ParseMode
from utils import Shop
from aiogram.dispatcher import FSMContext
import logging
import aiogram.utils.markdown as md

from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.contrib.middlewares.logging import LoggingMiddleware

bot = Bot(token=TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(LoggingMiddleware())

logging.basicConfig(level=logging.INFO)


@dp.message_handler(commands='start')
async def cmd_start(message: types.Message):
    await Shop.start.set()
    await bot.send_message(message.from_user.id, "Привет, я бот продуктового магазина Даня")
    markup_start = types.KeyboardButton("Перейти к покупкам")
    markup_catalog = types.KeyboardButton("Аккаунт")
    keyboard_markup_start = types.ReplyKeyboardMarkup(resize_keyboard=True).add(markup_start, markup_catalog)


@dp.message_handler(commands='catalog', state=Shop.catalog)
async def catalog(message: types.Message):
    await Shop.catalog.set()


# @dp.message_handler(state='*', commands='cancel')
# async def cancel_handler(message: types.Message, state: FSMContext):
#
#     current_state = await state.get_state()
#     if current_state is None:
#         return
#
#     logging.info('Cancelling state %r', current_state)
#
#     await state.finish()
#     await message.reply('Cancelled.', reply_markup=types.ReplyKeyboardRemove())
#
#
# @dp.message_handler(state=Shop.name)
# async def process_name(message: types.Message, state: FSMContext):
#     async with state.proxy() as data:
#         data['name'] = message.text
#
#     await Shop.age.set()
#     await message.reply("How old are you?")
#
#
# # Check age. Age gotta be digit
# @dp.message_handler(lambda message: not message.text.isdigit(), state=Shop.age)
# async def process_age_invalid(message: types.Message):
#     await message.reply("Age gotta be a number.\nHow old are you? (digits only)")
#
#
# @dp.message_handler(lambda message: message.text.isdigit(), state=Shop.age)
# async def process_age(message: types.Message, state: FSMContext):
#     # Update state and data
#     await Shop.gender.set()
#     await state.update_data(age=int(message.text))
#
#     # Configure ReplyKeyboardMarkup
#     markup = types.ReplyKeyboardMarkup(resize_keyboard=True, selective=True)
#     markup.add("Male", "Female")
#     markup.add("Other")
#
#     await message.reply("What is your gender?", reply_markup=markup)
#
#
# @dp.message_handler(lambda message: message.text not in ["Male", "Female", "Other"], state=Form.gender)
# async def process_gender_invalid(message: types.Message):
#     return await message.reply("Bad gender name. Choose your gender from the keyboard.")
#
#
# @dp.message_handler(state=Form.gender)
# async def process_gender(message: types.Message, state: FSMContext):
#     async with state.proxy() as data:
#         data['gender'] = message.text
#
#         markup = types.ReplyKeyboardRemove()
#
#         await bot.send_message(
#             message.chat.id,
#             md.text(
#                 md.text('Hi! Nice to meet you,', md.bold(data['name'])),
#                 md.text('Age:', md.code(data['age'])),
#                 md.text('Gender:', data['gender']),
#                 sep='\n',
#             ),
#             reply_markup=markup,
#             parse_mode=ParseMode.MARKDOWN,
#         )
#
#     # Finish conversation
#     await state.finish()


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
